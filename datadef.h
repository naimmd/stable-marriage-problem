/* 
 * File:   datadef.h
 * Author: Naim
 *
 * Created on May 18, 2016, 1:50 PM
 */

#ifndef DATADEF_H
#define	DATADEF_H

typedef struct prob {
    unsigned int nrMan;
    unsigned int nrWoman;
    unsigned int nrEdges;

    unsigned int *indicesMan;
    unsigned int *indicesWoman;

    unsigned int *mansPriorities;
    unsigned int *mansEvalbyWoman;

    unsigned int *womansPriorities;
} ManWomanGraph;

#endif	/* DATADEF_H */

