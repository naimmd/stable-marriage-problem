#include "memoryManager.h"
#include "bipartiteIO.h"
#include "assert.h"
#include "stdio.h"
#include "stdlib.h"

int main(int argc, char**argv) {


    unsigned int nrMan = 1;
    printf("\nEnter n: ");
    int retCode = scanf("%u", &nrMan);

    //Allocate Graph on host
    CompleteBipartiteGraph *hostgraph = allocateComBipartite(nrMan, nrMan);

    //write generated Graph on host
    char *name = writeRanks(hostgraph); // return an allocated name; bad practice ; delete it at the end

    printf("Before %p \n", hostgraph);
    //displayBipartite(hostgraph);

    //GO Green!
    freeBipartite(hostgraph);
    printf("After  %p \n", hostgraph);

    free(name);
    return 0;
}
