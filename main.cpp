#include <omp.h>

#include"dataGenerator.h"
#include"graphChecker.h"
#include"graphIO.h"
#include"memoryManager.h"

#include"hostdriver.h"
#include"iostream"
#include"time.h"
using namespace std;

#define NUM_THREAD 72

static void callSeqGenerator() {




    double currTime = omp_get_wtime();
   
    unsigned int num_man;
    printf("Enter num_man: ");
    scanf("%u", &num_man); 
    //Generate a graph
    ManWomanGraph* graph = generateGraph_Seq(num_man, num_man);

    currTime = omp_get_wtime() - currTime;

    std::cout << "Time to generate (sequentially):" << currTime << " seconds " << std::endl;

    displaySparse(graph);

    //displayGraph(graph);

    //Check if it's a legal stable marriage graph
    if (checkManWomanGraph(graph, NUM_THREAD)) {
        std::cout << "Valid Graph for stable marriage" << std::endl;
    } else {
        std::cout << "Error: Invalid Graph Format" << std::endl;
    }

    freeGraph(graph);
}

int main(int argc, char**argv) {


    double currTime = omp_get_wtime();

    unsigned int num_man;
    printf("Enter num_man: ");
    scanf("%u", &num_man); 


    //Generate a graph in parallel
    //
    //
    ManWomanGraph* graph = generateGraph(num_man, num_man, NUM_THREAD);

    currTime = omp_get_wtime() - currTime;

    std::cout << "Time to generate:" << currTime << " seconds " << std::endl;

    displaySparse(graph);

    //displayGraph(graph);

    //Check if it's a legal stable marriage graph
    if (checkManWomanGraph(graph, NUM_THREAD)) {
        std::cout << "Valid Graph for stable marriage" << std::endl;
    } else {
        std::cout << "Error: Invalid Graph Format" << std::endl;
    }


    currTime = omp_get_wtime();
    //write it in a file
    char* fileName = writeManWomanGraphInBinary(graph);

    std::cout << "File:" << fileName << std::endl;
    currTime = omp_get_wtime() - currTime;

    std::cout << "Time to write:" << currTime << " seconds " << std::endl;

    //Free the graph
    freeGraph(graph);

    //Read input graph from file


    std::cout << "Going to read from " << fileName << std::endl;
    currTime = omp_get_wtime();

    ManWomanGraph* inputGraph = readManWomanGraphInBinary(fileName);

    currTime = omp_get_wtime() - currTime;

    std::cout << "Time to read:" << currTime << " seconds " << std::endl;

    displaySparse(inputGraph);
    
    //displayGraph(inputGraph);

    //Go Green!
    freeGraph(inputGraph);

    free(fileName);
    return 0;
}
