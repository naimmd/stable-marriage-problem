#!/bin/bash
CUDAVERSION=7.0

CC = g++
CFLAGS = -I. -O3 -std=c++11 -fopenmp
LIBS = -fopenmp
# -lpthread
DEPS = datadef.h dataGenerator.h  graphChecker.h  graphIO.h memoryManager.h bipartiteIO.h
OBJS = dataGenerator.o  graphChecker.o  graphIO.o  main.o  manageMemory.o dataGenSeq.o bipartiteIO.o
OBJS_3 = genBipartiteGraph.o manageMemory.o bipartiteIO.o


HFLAG= -D DISPLAY_DETAILS


CU_CC=/usr/local/cuda-$(CUDAVERSION)/bin/nvcc
CU_FLAGS = -I/usr/local/cuda-$(CUDAVERSION)/include -O3 -arch sm_35  -Xptxas -dlcm=ca
CU_LIBS = -L/usr/local/cuda-$(CUDAVERSION)/lib64 -lcudart
CU_DEPS = hostdriver.h  kernelsdef.h datadef.h bipartiteIO.h
CU_OBJS = hostdriver.o marriageKernels.o cudaMain.o graphChecker.o  graphIO.o manageMemory.o hostDevMem.o completeBipartiteMarriageKernels.o bipartiteIO.o

DFLAGS= -D RUNONGPU 
FLAGS_TO_HOST_COMPILER= -Xcompiler


#Generate Bipartite Graph
EXEC_1=generateBipartiteGraph

EXEC_2=runStableMarriage 

#Generate Complete Bipartite Graph
EXEC_3=generateCompleteBipartite 


all:$(EXEC_1) $(EXEC_2) $(EXEC_3)
	
%.o: %.cu $(CU_DEPS)
	$(CU_CC) -o $@ -c $< $(CU_FLAGS) $(DFLAGS)

%.o: %.cpp $(DEPS)
	$(CC) -o $@ -c $< $(CFLAGS) 
# $(HFLAG) 

$(EXEC_1): $(OBJS)
	@echo About to make distribution $(EXEC_1)
	$(CC) -o $@ $^ $(LIBS)

$(EXEC_2): $(CU_OBJS)
	@echo About to make distribution $(EXEC_2)
	$(CU_CC) -o $@ $^ $(FLAGS_TO_HOST_COMPILER) $(LIBS) $(CU_LIBS)

$(EXEC_3): $(OBJS_3)
	@echo About to make distribution $(EXEC_3)
	$(CC) -o $@ $^ $(LIBS)
clean:
	rm -f *.o $(EXEC_1) $(EXEC_2) $(EXEC_3)
