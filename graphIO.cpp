#include"graphIO.h"
#include"memoryManager.h"
#include <ios>
#include <fstream>
#include <string.h>
#include"stdlib.h"
#include "iostream"

char* writeManWomanGraphInBinary(ManWomanGraph* graph) {

    std::string fileName = "manWomanGraph_";

    fileName.append(std::to_string(graph->nrMan)).append("_");
    fileName.append(std::to_string(graph->nrWoman)).append("_");
    fileName.append(std::to_string(graph->nrEdges)).append(".dat");

    std::cout << "Writing to " << fileName << std::endl;

    std::ofstream ofs;
    ofs.open(fileName.c_str(), std::ios::out | std::ios::binary);

    if (graph) {
        ofs.write((char*) &graph->nrMan, sizeof (unsigned int));
        ofs.write((char*) &graph->nrWoman, sizeof (unsigned int));
        ofs.write((char*) &graph->nrEdges, sizeof (unsigned int));
        if (graph->indicesMan && graph->indicesWoman && graph->mansPriorities
                && graph->mansEvalbyWoman && graph->womansPriorities) {

            ofs.write((char*) &graph->indicesMan[0], sizeof (unsigned int)*(graph->nrMan + 1));
            ofs.write((char*) &graph->indicesWoman[0], sizeof (unsigned int)*(graph->nrWoman + 1));

            ofs.write((char*) &graph->mansPriorities[0], sizeof (unsigned int) * graph->nrEdges);
            ofs.write((char*) &graph->mansEvalbyWoman[0], sizeof (unsigned int) * graph->nrEdges);
            ofs.write((char*) &graph->womansPriorities[0], sizeof (unsigned int) * graph->nrEdges);
        }
    }
    ofs.close();

    char* nameToReturn = (char*) malloc(sizeof (char)*(fileName.size() + 1));
    strcpy(nameToReturn, fileName.c_str());

    printf("Wrote to %s\n", fileName.c_str());
    return nameToReturn;
}

ManWomanGraph* readManWomanGraphInBinary(const char* fileName) {

    
    ManWomanGraph* graph = NULL;

    std::ifstream ifs;
    ifs.open(fileName, std::ios::in | std::ios::binary);
    if (ifs.is_open()) {

        unsigned int nrMan;
        unsigned int nrWoman;
        unsigned int nrEdges;

        ifs.read((char*) &nrMan, sizeof (unsigned int));
        ifs.read((char*) &nrWoman, sizeof (unsigned int));
        ifs.read((char*) &nrEdges, sizeof (unsigned int));

        //Allocate memory for the graph
        graph = allocateGraph(nrMan, nrWoman, nrEdges);


        printf("\nReading File with %d Men, %d Women and %d Edges\n", graph->nrMan, graph->nrWoman, graph->nrEdges);

        if (graph->indicesMan && graph->indicesWoman && graph->mansPriorities
                && graph->mansEvalbyWoman && graph->womansPriorities) {

            ifs.read((char*) &graph->indicesMan[0], sizeof (unsigned int)*(graph->nrMan + 1));
            ifs.read((char*) &graph->indicesWoman[0], sizeof (unsigned int)*(graph->nrWoman + 1));

            ifs.read((char*) &graph->mansPriorities[0], sizeof (unsigned int) * graph->nrEdges);
            ifs.read((char*) &graph->mansEvalbyWoman[0], sizeof (unsigned int) * graph->nrEdges);

            ifs.read((char*) &graph->womansPriorities[0], sizeof (unsigned int) * graph->nrEdges);
        } else {
            printf("ERROR: One of the array in NULL\n");
        }
    } else {
        printf("\nERROR:Can't open inputGraph\n");
    }
    return graph;
}
