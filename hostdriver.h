/* 
 * File:   hostdriver.h
 * Author: Naim
 *
 * Created on May 22, 2016, 7:18 PM
 */

#ifndef HOSTDRIVER_H
#define	HOSTDRIVER_H

#include "datadef.h"


void findStableMarriage(ManWomanGraph* graph);
void findBipartiteMarriage(); //to handle special instance

#endif	/* HOSTDRIVER_H */

