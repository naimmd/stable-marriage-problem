#include <stdlib.h>
#include <set>
#include <string.h>
#include <assert.h>
#include <malloc.h>
#include "unordered_set"
#include "unordered_map"
#include "time.h"
#include "omp.h"
#include "math.h"

#include "dataGenerator.h"

#include"iostream"
using namespace std;

#define SZ_PER_STATE_BUFFER 64
/*
 * Generate Graph with given nrMan and nrWoman; we don't know the nrEdges until 
 * we generate 
 */
ManWomanGraph* generateGraph(unsigned int nrMan, unsigned int nrWoman, unsigned int nrThreads) {


    
    //Setup local random generator for each thread

    struct random_data* rand_states;
    char* rand_statebufs;
    
    //allocate memory for random_states and rand_statebuffers

    rand_states = (struct random_data*)calloc(nrThreads, sizeof(struct random_data));

    
    rand_statebufs = (char*)calloc(nrThreads, SZ_PER_STATE_BUFFER);
    
    for (int th = 0; th< nrThreads; th++) {

        initstate_r(random(), &rand_statebufs[th], SZ_PER_STATE_BUFFER, &rand_states[th]);

    }


    double start = omp_get_wtime();

    double currTime = omp_get_wtime();

    // allocate and clear memory
    ManWomanGraph *graph = (ManWomanGraph*) malloc(sizeof (ManWomanGraph));
    graph->nrMan = nrMan;
    graph->nrWoman = nrWoman;
    graph->indicesMan = (unsigned int*) malloc((nrMan + 1) * sizeof (unsigned int));
    graph->indicesWoman = (unsigned int*) malloc((nrWoman + 1) * sizeof (unsigned int));

    memset(graph->indicesMan, 0, sizeof (unsigned int)*(nrMan + 1));
    memset(graph->indicesWoman, 0, sizeof (unsigned int) *(nrWoman + 1));

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to initialize:" << currTime << " seconds " << std::endl;

    //omp_set_dynamic(0); // Explicitly disable dynamic teams


#ifdef DISPLAY_DETAILS
    std::cout << "Before Parallel Construct: " << "|P| = " << omp_get_num_procs() << "|T| = " << omp_get_num_threads() << std::endl;

    //Check how many thread we got 
#pragma omp parallel 
    {
#pragma omp single
        cout << "threads=" << omp_get_num_threads() << endl;
    }
#endif


    currTime = omp_get_wtime();

    //Objective: For each man generate a list of preferred woman of length log(nrWoman) to 2*log(nrWoman); i.e 1.5log(nrWoman) on average


    
    //First, generate #choices for each man
    int minNrChoice = (int) log2(nrWoman);

#pragma omp parallel num_threads(nrThreads)
    {
        int threadId= omp_get_thread_num();

#pragma omp for
        for (int i = 0; i < nrMan; i++) {
            
            int currNum;
            random_r(&rand_states[threadId], &currNum);
            int nrChoice = minNrChoice + currNum % minNrChoice; //  maximum  2*log(nrWoman)
        
            //int nrChoice = minNrChoice + rand() % minNrChoice; //  maximum  2*log(nrWoman)

            //NOTE:: number of choice for Man i is stored in index (i+1) 
            graph->indicesMan[i + 1] = nrChoice;

            if ( i==0 && threadId==0) {
                std::cout << "*|P| = " << omp_get_num_procs() << "  |T| = " << omp_get_num_threads() << " i:" << i << std::endl;
            }
        }
    }
    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to decide #choices:" << currTime << " seconds " << std::endl;



#ifdef DISPLAY_DETAILS
    //Print #choice per man    
    for (int i = 0; i <= nrMan; i++) {
        std::cout << graph->indicesMan[i] << " ";
    }
    std::cout << std::endl;

#endif

    //Prefix sum to get position of woman list for each man
    for (int i = 0; i < nrMan; i++) {
        graph->indicesMan[i + 1] += graph->indicesMan[i];
    }

#ifdef DISPLAY_DETAILS
    //After prefix sum
    for (int i = 0; i <= nrMan; i++) {
        std::cout << graph->indicesMan[i] << " ";
    }
#endif


    unsigned int szEdgelist = graph->indicesMan[nrMan];

    std::cout << std::endl << "Generating a graph with " << nrMan << " men and " << nrWoman << " women" << std::endl;

    std::cout << "size of Edge list = " << szEdgelist << " * 2" << std::endl;

    graph->mansPriorities = (unsigned int*) malloc(szEdgelist * sizeof (unsigned int));


    std::cout << "Start generating choices for each man" << std::endl;

    currTime = omp_get_wtime();

    
    //For each man generate preferred women 
    
#pragma omp parallel num_threads(nrThreads)
    {
        int threadId = omp_get_thread_num();
#pragma omp for 
        for (int i = 0; i < nrMan; i++) {

            std::unordered_set<int> myset;
            std::unordered_set<int>::iterator it;

            int nrElement = graph->indicesMan[i + 1] - graph->indicesMan[i ];
            int currNum;

            while (myset.size() < nrElement) {
               
                random_r(&rand_states[threadId], &currNum);

                int womanId = currNum % nrWoman;
                myset.insert(womanId);
            }


            if (nrElement != myset.size())
                std::cout << "ERROR: Don't have enough choices for Man " << i << std::endl;

            it = myset.begin();

            for (int j = graph->indicesMan[i]; j < graph->indicesMan[i + 1]; j++) {
                graph->mansPriorities[j] = *it;
                it++;
            }

            myset.clear();

            if (omp_get_thread_num() == 0 && i == 0) {
                std::cout << "**|P| = " << omp_get_num_procs() << "  |T| = " << omp_get_num_threads() << " i:" << i << std::endl;
            }

        }
    }


    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to generate choices:" << currTime << " seconds " << std::endl;



#ifdef DISPLAY_DETAILS
    //print list of woman for each man
    std::cout << std::endl;
    for (int i = 0; i < nrMan; i++) {
        std::cout << i << " : ";
        for (int j = graph->indicesMan[i]; j < graph->indicesMan[i + 1]; j++) {
            std::cout << graph->mansPriorities[j] << " ";
        }
        std::cout << std::endl;
    }
#endif


    std::cout << "Count how many times each woman appeared" << std::endl;

#ifdef DISPLAY_DETAILS
    //print
    std::cout << std::endl;
    for (int i = 0; i <= nrWoman; i++) {
        std::cout << graph->indicesWoman[i] << " ";
    }
    std::cout << std::endl;
#endif

    currTime = omp_get_wtime();

    // for each woman count how many times they appeared in the lists of men

#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrMan; i++) {
        for (int j = graph->indicesMan[i]; j < graph->indicesMan[i + 1]; j++) {
            int womanId = graph->mansPriorities[j];
            __sync_fetch_and_add(&graph->indicesWoman[womanId + 1], 1);
            //graph->indicesWoman[womanId + 1]++;
        }
    }


    currTime = omp_get_wtime() - currTime;

    std::cout << "Time to count each woman:" << currTime << " seconds " << std::endl;

#ifdef DISPLAY_DETAILS
    //print
    std::cout << std::endl;
    for (int i = 0; i <= nrWoman; i++) {
        std::cout << graph->indicesWoman[i] << " ";
    }
    std::cout << std::endl;
#endif

    currTime = omp_get_wtime();

    //prefix sum to get the position of neighborlist (ie list of men) for each woman
    for (int i = 0; i < nrWoman; i++) {
        graph->indicesWoman[i + 1] += graph->indicesWoman[i];
    }

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to compute prefix sum :" << currTime << " seconds " << std::endl;


#ifdef DISPLAY_DETAILS
    //print
    std::cout << std::endl;
    for (int i = 0; i <= nrWoman; i++) {
        std::cout << graph->indicesWoman[i] << " ";
    }
    std::cout << std::endl;
#endif    

    unsigned int sumWomanfreq = graph->indicesWoman[nrWoman];

    assert(szEdgelist == sumWomanfreq);

    graph->nrEdges = szEdgelist;

    graph->womansPriorities = (unsigned int*) malloc(sumWomanfreq * sizeof (unsigned int));



    unsigned int *womanPtrs = (unsigned int*) malloc(sizeof (unsigned int)*(nrWoman + 1));

    memcpy(womanPtrs, graph->indicesWoman, (nrWoman + 1) * sizeof (unsigned int));

#ifdef DISPLAY_DETAILS

    //print
    std::cout << "temporaryPtrs:" << std::endl;
    for (int i = 0; i <= nrWoman; i++) {
        std::cout << womanPtrs[i] << " ";
    }

#endif


    std::cout << "Copy preferred men into list of each woman" << std::endl;

    currTime = omp_get_wtime();
    //copy list of men into each woman lists
#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrMan; i++) {
        for (int j = graph->indicesMan[i]; j < graph->indicesMan[i + 1]; j++) {

            int womanId = graph->mansPriorities[j];

            // go to the list of particular woman and put the man 'i' there
            int position = __sync_fetch_and_add(&womanPtrs[womanId], 1);
            graph->womansPriorities[position] = i; // i is the manId

        }
    }


    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to build list(of men) for women:" << currTime << " seconds " << std::endl;

#ifdef DISPLAY_DETAILS

    //print
    std::cout << std::endl;
    std::cout << std::endl;
    for (int i = 0; i < nrWoman; i++) {
        std::cout << i << " : ";
        for (int j = graph->indicesWoman[i]; j < graph->indicesWoman[i + 1]; j++) {
            std::cout << graph->womansPriorities[j] << " ";
        }
        std::cout << std::endl;
    }
#endif


    // shuffle men in the list  of each woman



    std::cout << "Find ranks of each man in women lists" << std::endl;

    currTime = omp_get_wtime();

    omp_lock_t *locksForMan = (omp_lock_t*) malloc(nrMan * sizeof (omp_lock_t));

#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrMan; i++) {
        omp_init_lock(&locksForMan[i]);
    }


    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to create locks:" << currTime << " seconds " << std::endl;

    std::cout << "Created lock for each man" << std::endl;

    currTime = omp_get_wtime();


    //Construct rank evaluation of each man by neigboring women

    unordered_map<int, int>* evalMap = new unordered_map<int, int>[nrMan];



#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrWoman; i++) {
        int rankOffset = graph->indicesWoman[i];
        for (int j = graph->indicesWoman[i]; j < graph->indicesWoman[i + 1]; j++) {

            int manId = graph->womansPriorities[j];

            // multiple threads  may try to update the same map for "manId"

            omp_set_lock(&locksForMan[manId]);

            evalMap[manId][i] = j - rankOffset;

            omp_unset_lock(&locksForMan[manId]);

        }
    }

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to find ranks:" << currTime << " seconds " << std::endl;



    currTime = omp_get_wtime();

#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrMan; i++) {
        omp_destroy_lock(&locksForMan[i]);
    }

    //Free memory occupied by locks

    free(locksForMan);

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to destroy locks:" << currTime << " seconds " << std::endl;



    std::cout << "Copy ranks of man into corresponding array" << std::endl;

    graph->mansEvalbyWoman = (unsigned int*) malloc(szEdgelist * sizeof (unsigned int));


    currTime = omp_get_wtime();

#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrMan; i++) {
        for (int j = graph->indicesMan[i]; j < graph->indicesMan[i + 1]; j++) {
            int womanId = graph->mansPriorities[j];
            graph->mansEvalbyWoman[j] = evalMap[i][womanId];
        }
    }


    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to copy ranks:" << currTime << " seconds " << std::endl;


    currTime = omp_get_wtime();

#pragma omp parallel for num_threads(nrThreads)
    for (int i = 0; i < nrMan; i++) {
        evalMap[i].clear();
    }

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to clear maps:" << currTime << " seconds " << std::endl;


    currTime = omp_get_wtime();

    //Free temporary buffers
    free(womanPtrs);
    free(rand_statebufs);
    free(rand_states);

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to free:" << currTime << " seconds " << std::endl;


    currTime = omp_get_wtime();
    delete [] evalMap;

    currTime = omp_get_wtime() - currTime;
    std::cout << "Time to delete maps:" << currTime << " seconds " << std::endl;


    start = omp_get_wtime() - start;
    std::cout << "Total:" << start << " seconds " << std::endl;

    return graph;

}

